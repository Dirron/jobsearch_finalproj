<?php

$main = file_get_contents("templates/create-posting.html");

include "tiles/core-js.php";
include "tiles/footer.php";
include "tiles/head.php";
include "tiles/navigation-bar.php";
include "helper/communication-helper.php";

include "tiles/banner.php";

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

$has_company = check_has_company();
$server_response = array();

$server_response = array_merge($server_response, $has_company);

if (!isset($_SESSION["email"])) {

    include "helper/login-helper.php";

    login_with_destination("create-posting.php", "Please log in to create a posting.");
    exit();

} else if ($has_company && isset($_POST["submit"])) {
    $result = create_job_posting();

    $server_response = array_merge($server_response, $result);
}

$main = add_vars_to_js($main, $server_response);

echo $main;


function create_job_posting() {
    include_once "database/job-query.php";
    include_once "database/user-query.php";

    $company_id = UserQuery::get_company_by_user_email($_SESSION["email"])["id"];

    if (!isset($_POST["job-title"]) || !isset($_POST["job-posting-type"]) || !isset($_POST["job-description"])) {
        header("Location: create-posting.php");
    }

    $result = JobQuery::save(
        $company_id,
        $_POST["job-title"],
        $_POST["job-posting-type"],
        nl2br(htmlentities($_POST["job-description"], ENT_QUOTES)),
        $_POST["location"] ?? null
    );

    return array("success" => $result ? true : false);
}

function check_has_company() {
        // check that user has a company
        include "database/user-query.php";

        $email = $_SESSION["email"];
        $has_company = is_null(UserQuery::get_company_by_user_email($email)["company_name"]) ? false : true;

        $data = array("has_company" => $has_company);

        return $data;
}