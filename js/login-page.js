$(document).ready(function () {
    // Login.php: Sign up button
    $("#center-area").on("click", "#sign-up", function () {

        if (!$("#email-confirm-group").length && !$("#password-confirm-group").length) {
            var title = `<h1 class="mb-4 text-center" id="create-account">Create a new account</h1>`;
            var first_name = `<div id="fname-group">
                            <label for="fname">First name</label>
                            <input type="text" class="form-control off-white center" name="fname" id="fname">
                            <br/>
                            </div>`;
            var last_name = `<div id="lname-group">
                            <label for="lname">Last name</label>
                            <input type="text" class="form-control off-white center" name="lname" id="lname" required>
                            <br/>
                            </div>`;
            var confirm_email = `<div id="email-confirm-group">
                            <label for="email">Confirm email</label>
                            <input type="text" class="form-control off-white center" name="email-confirm" id="email-confirm" required>
                            <br/>
                            </div>`;

            var confirm_pass = `<div id="password-confirm-group">
                            <label for="password">Confirm password</label>
                            <input type="password" class="form-control off-white center" name="password-confirm" id="password-confirm" required>
                            <br/>
                            </div>`;

            $("#center-area").prepend(title).hide().fadeIn();

            $("#email-group").before(first_name).hide().slideToggle("fast");
            $("#fname-group").after(last_name).hide().slideToggle("fast");

            $("#email-group").after(confirm_email).hide().slideToggle("fast");
            $("#password-group").after(confirm_pass).hide().slideToggle("fast");

            $("#account_action").attr("value", "register");

            $("#login").replaceWith(`<input type="button" class="btn" id="login" value="Already have an account?">`);
            $(this).replaceWith(`<input type ="submit" class="btn btn-primary" id="sign-up" value="Submit">`);
        }
    });

    // Login.php: Login button
    $("#center-area").on("click", "#login", function () {
        if ($("#email-confirm-group").length || $("#password-confirm-group").length) {
            $("#email-confirm-group").slideToggle("fast", function () {
                $(this).remove();
            });
            $("#password-confirm-group").slideToggle("fast", function () {
                $(this).remove();
            });
            $("#fname-group").slideToggle("fast", function () {
                $(this).remove();
            });
            $("#lname-group").slideToggle("fast", function () {
                $(this).remove();
            });

            $("#create-account").fadeOut("fast", function () { $(this).remove() });

            $("#account_action").attr("value", "login");

            $("#sign-up").replaceWith(`<input type ="button" class="btn" id="sign-up" value="Sign up">`);
            $(this).replaceWith(`<input type="submit" class="btn btn-primary" id="login" value="Login">`);
        }
    });

    //////////////////////
    ////ERROR HANDLING////
    //////////////////////

    // all possible login page errors
    var errors = {
        "userexists":"There is already an account with the email",
        "invalidemail":"An invalid email was entered",
        "emailmismatch":"Please make sure the entered emails are the same.",
        "passwordmismatch":"Please make sure the entered passwords are the same.",
        "missing":"Please make sure all fields are filled.",
        "retry":"Unknown error. Please try again."
    };
    var error = getUrlParameter("error");

    // create an error message under an element
    function error_message(id, message, element) {
        if (!$("#" + id).length) {
            var p_open = `<p id="` + id + `" class="text-danger">`;
            var p_close = `</p>`;
            $(element).after(p_open + message + p_close);
        }
    }

    // toggle submit based on errors
    function toggle_submit() {
        var disabled = false;
        var test = "";
        Object.keys(errors).forEach(function(key) {
            if ($("#" + key).length) {
                disabled = true;
            }
        })
        $("#sign-up").prop("disabled", disabled);
    }

    // check if email and password confirmations match
    function confirm_matching() {
        if ($("#email-confirm").val().length) {
            if ($("#email-confirm").val() != $("#email").val()) {
                var message = errors["emailmismatch"];
                error_message("emailmismatch", message, "#email-confirm");
                toggle_submit();
            } else {
                $("#emailmismatch").remove();
                toggle_submit();
            }
        }
    }

    // display errors on load
    if (error == "userexists") {
        var email = getUrlParameter("email");
        var message = errors["userexists"] + " " + email + "!";
        $("#sign-up").trigger("click");
        error_message("userexists", message, "#email");
    } else if (error == "invalidemail") {
        var message = errors["invalidemail"];
        $("#sign-up").trigger("click");
        error_message("invalidemail", message, "#email");
    } else if (error == "missing") {
        var message = "<br/>" + errors["missing"];
        $("#sign-up").trigger("click");
        error_message("missing", message, "#sign-up");
    } else if (error == "emailmismatch" || error == "passwordmismatch") {
        confirm_matching();
    }

    // ON CHANGE REMOVE ERRORS start //
    $("input[type='text'").on("input", function() {
        $("#missing").remove();
        toggle_submit();
    })
    $("#email").on("input", function() {
        $("#userexists").remove();
        $("#invalidemail").remove();
    });
    // ON CHANGE REMOVE ERRORS end //


    // CONFIRMATION VALIDATIONS start //
    // validate email confirm
    $("#center-area").on("input", "#email, #email-confirm", function () {
        confirm_matching();
    });

    // validate password confirm
    $("#center-area").on("input", "#password, #password-confirm", function () {
        if ($("#password-confirm").val().length) {
            if ($("#password-confirm").val() != $("#password").val()) {
                var message = errors["passwordmismatch"];
                error_message("passwordmismatch", message, "#password-confirm");
                toggle_submit();
            } else {
                $("#passwordmismatch").remove();
                toggle_submit();
            }
        }
    });
    // CONFIRMATION VALIDATIONS end //

    $("#login-form").before(`<div class="text-center text-danger">${server_response.login_message}</div>`);

    toggle_submit();
});
