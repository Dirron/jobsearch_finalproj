$(document).ready(function () {
    $("#title").html(post_vars.title);
    $("#sub-header").html(`(${post_vars.sub_header})`);
    $('input[name="title"]').val(post_vars.title);
    $('input[name="sub_header"]').val(post_vars.sub_header);
    $('input[name="posting_id"]').val(post_vars.posting_id);

    if (!post_vars.upload_success) {
        $("#resume-form").show();
    } else {
        let success_message = `<div class="text-center">Your resume has been successfully uploaded!</div>`;
        $("#resume-form").after(success_message);
    }
});