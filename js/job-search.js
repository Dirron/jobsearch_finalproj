$(document).ready(function () {

    // ON READY: ajax call to get json results
    let uri = URI(window.location);
    let params = uri.search(true);

    get_job_types();
    add_hidden();
    if (uri.hasQuery("job") || uri.hasQuery("location")) {
        search_results()
            .then(filter_salary)
            .then(show_results);
    }

    // END READY

    // search
    function search_results() {
        let current = new URI(window.location);
        if (current.hasQuery("job") || current.hasQuery("location")) {
            const search_string = getUrlParameter("job");
            const location = getUrlParameter("location");

            return jQuery.getJSON(
                "/handlers/get-job-postings.php",
                {job: search_string, location: location},
                function (data) {
                    let results = [];
                    let count = 0;

                    $.each(data, function (key, value) {
                        results.push(format_result(value));
                        count++;
                    });

                    $("#results").html(results.join(""));
                });
        }
    }

    // display results
    function show_results() {

        synchronous_filter();

        let results = document.getElementsByName("result");

        for (let i = 0; i < results.length; i++) {
            if (results[i].dataset.show == "true") {
                results[i].style.display = "";
            }
        }
    }

    // create html formatted result
    function format_result(job_posting) {

        let title = job_posting.title;
        let posting_type = job_posting.posting_type;
        let description = job_posting.description;
        let date_modified = job_posting.date_last_modified;
        let company_id = job_posting.company_id;
        let posting_id = job_posting.id;
        let sub_header = format_sub_header(job_posting);

        let count = 0;

        return `<div class="row mb-3 border-bottom" name="result" id="result-${count}" style="display: none" data-show="false">
                <div class="col-md-12">
                    <h3>${title}</h3>
                    <p class="text-info" id="sub-header">
                        ${sub_header}
                    </p>
                    <input type="hidden" name="job_posting_type" value="${posting_type}">
                    <input type="hidden" name="company_id" value="${company_id}">
                    <input type="hidden" name="date_modified" value="${date_modified}">
                    <form action="../submit-application.php" method="post" name="result-expand" style="display: none">
                        <p name="description">${description}</p>
                        <input type="hidden" name="title" value="${title}">
                        <input type="hidden" name="sub_header" value="${sub_header}">
                        <input type="hidden" name="posting_id" value="${posting_id}">
                        <input type="submit" class="btn btn-primary" value="Apply Now">
                    </form>
                </div>
                </div>`
    }

    function format_sub_header(job_posting) {
        let string = "";

        string += job_posting.company_name;

        if (job_posting.location) {
            string += " | " + job_posting.location;
        }

        return string;
    }

    // filters without ajax call
    function synchronous_filter() {
        filter_job_posting_type();
        filter_location();
    }

    // add salary query on checkbox checked
    $('input[name="salary-filter"]').on("change", function () {
        let uri = new URI(window.location);

        if ($(this).is(":checked")) {
            let salary_range = $(this).val().split(",");

            save_checked($(this).attr("name"), $(this).attr("id"));

            uri.removeQuery("salary_min");
            uri.removeQuery("salary_max");
            uri.addQuery({salary_min: salary_range[0], salary_max: salary_range[1]});
            window.location = uri.toString();
        } else {
            uri.removeQuery("salary_min");
            uri.removeQuery("salary_max");

            save_checked($(this).attr("name"), "");

            window.location = uri.toString();
        }
    });

    // mark for showing
    function filter_salary() {
        return $.ajax({
            dataType: "json",
            url: "handlers/get-job-postings.php",
            data: {salary_min: params.salary_min, salary_max: params.salary_max},
            success: function (data) {
                $.each(data, function (key, val) {
                    $(`[name="company_id"][value="${val.company_id}"]`).parent().parent().attr("data-show", true);
                })
            },
            error: function(jqxhr, status, exception) {
                console.log("Exception: ", exception);
            }
        });
    }

    // add job posting type query on checkbox checked
    $("#job-posting-type-filters").on("change", 'input[name="job_posting_type_filter"]', function () {
        let uri = new URI(window.location);

        if ($(this).is(":checked")) {
            let value = $(this).val();

            save_checked($(this).attr("name"), $(this).attr("id"));

            uri.removeQuery("job_posting_type");
            uri.addQuery({job_posting_type: value});
            window.location = uri.toString();
        } else {
            save_checked($(this).attr("name"), "");

            uri.removeQuery("job_posting_type");
            window.location = uri.toString();
        }
    });

    // mark for hiding
    function filter_job_posting_type() {
        if (uri.hasQuery("job_posting_type")) {
            let job_posting_type = params.job_posting_type;

            $("#results").children().each(function (key, value) {
                let type = $(value).find(`[name="job_posting_type"]`);
                if (type.val() != job_posting_type) {
                    $(value).attr("data-show", false);
                }
            })
        }
    }

    // hide non-matching locations
    function filter_location() {
        if (uri.hasQuery("location")) {
            let location = params.location.toLowerCase();

            $("#results").children().each(function (key, value) {
                let loc = $(value).find("#sub-header");
                if (!loc.html().toLowerCase().includes(location)) {
                    $(value).attr("data-show", false);
                }
            })
        }
    }

    // get job posting types for filter menu
    function get_job_types() {
        return $.ajax({
            dataType: "json",
            url: "handlers/get-job-postings.php",
            data: {job_posting_types:""},
            success: function (data) {
                let job_posting_types = [];
                $.each(data, function (key, value) {
                    let formatted = `<label><input type="checkbox" id="job-posting-type-${value.id}" name="job_posting_type_filter" value="${value.id}">${value.description}</label><br/>`;
                    job_posting_types.push(formatted);
                })

                $("#job-posting-type-filters").append(job_posting_types);
                recheck();
            }
        });
    }

    // add hidden input so filter remains on new search
    function add_hidden() {
        $.each(params, function (key, value) {
            if (key != "job" && key != "location") {
                let hidden = `<input type="hidden" name="${key}" value="${value}">`;
                $("#job-search-form").append(hidden);
            }
        })
    }

    // display left border on result hover
    $("#results").on("mouseover", '[name="result"]', function () {
        $(this).addClass("result-hover");
    }).on("mouseleave", '[name="result"]', function () {
        $(this).removeClass("result-hover");
    })

    // expand result on click
    $("#results").on("click", '[name="result"]', function () {
        $("#results").find('[name="result-expand"]').hide();
        $("#results").children().removeClass("result-select");

        $(this).find('[name="result-expand"]').show();
        $(this).addClass("result-select");
    });


});