$(document).ready(function () {
    get_job_types();

    // get job posting types for filter menu
    function get_job_types() {
        return $.ajax({
            dataType: "json",
            url: "handlers/get-job-postings.php",
            data: {job_posting_types:""},
            success: function (data) {
                let job_posting_types = [];
                $.each(data, function (key, value) {
                    let formatted = `<option id="job-posting-type-${value.id}" value="${value.id}">${value.description}</option>`;
                    job_posting_types.push(formatted);
                })

                $("#job-posting-type").append(job_posting_types);
            }
        });
    }

    if (server_response.success) {
        $("#main").html("<h4>Your job posting has been successfully created!</h4>");
    } else {
        $("#resume-form").show();
    }

    if (!server_response.has_company) {
        $("#register-company").show();
    } else {
        $("#submit").removeClass("disabled").removeAttr("disabled");
    }

});