// REMEMBER FILTERS BETWEEN LOADS
function get_checked() {
    if (localStorage.checked == null) {
        localStorage.checked = "{}";
    }

    if (localStorage.checked.length == 0) {
        return {};
    } else {
        return JSON.parse(localStorage.checked);
    }
}
function save_checked(name, identifier) {
    let storage = get_checked();

    storage[name] = identifier;

    localStorage.checked = JSON.stringify(storage);
}
function recheck() {
    let storage = get_checked();

    jQuery.each(storage, function (key, value) {
        if (value.length > 0) {
            $(`#${value}`).prop("checked", true);
        }
    });
}

$(document).ready(function () {
    let uri = new URI(window.location);
    let current = uri.removeQuery(/.*/).toString();

    if (!current.includes("job-search.php")) {
        localStorage.checked = "{}";
    }
})