$(document).ready(function () {
    $("#full-name").html(server_response.name);
    $("#email").html(server_response.email);
    $("#account-level").html(server_response.account_level);
    $("#registered-company").html(server_response.company_name);

    if (server_response.company_name == "n/a") {
        $("#register-company").show();
    } else {
        get_jobs_posted().then(count_applications);
    }

    function get_jobs_posted() {
        return $.ajax({
            dataType: "json",
            url: "handlers/get-job-postings.php",
            data: {company_id: server_response.company_id},
            success: function (data) {
                let count = 1;
                $.each(data, function (key, value) {
                    let location;

                    if (value.location) {
                        location = value.location;
                    } else {
                        location = "n/a";
                    }

                    let row = `<tr name="${value.title}">
                                    <th scope="row">${count}</th>
                                    <td>${value.title}</td>
                                    <td>${location}</td>
                                    <td name="responses"></td>
                                    <td><input type="checkbox" id="checkbox-${count}" value="${value.id}"></td>
                                </tr>`;
                    $("#jobs-posted-body").append(row);
                    count++;
                });

                $("#jobs-posted-table").show();
            }
        });
    }

    function count_applications() {
        return $.ajax({
            type: "post",
            dataType: "json",
            url: "account.php",
            data: {count_applications: "true", company_id: server_response.company_id},
            success: function (data) {
                $.each(data, function(key, value) {
                    $(`tr[name="${key}"]`).find('[name="responses"]').html(value);
                });
            },
            error: function(jqxhr, status, exception) {
                console.log("Exception: ", exception);
            }
        });
    }

    function select_all(is_checked) {
        $('td > input[type="checkbox"]').prop("checked", is_checked).trigger("change");
    }

    //function get_selected_job_posting_ids() {
    $('#jobs-posted-table').on("change", 'td > input[type="checkbox"]', function () {
        let posting_id = $(this).val();

        if (this.checked) {
            $("#jobs-posted-table").append(`<input type="hidden" id="job-posting-${posting_id}" name="job_posting_ids[]" value="${posting_id}">`);
        } else {
            $(`#job-posting-${posting_id}`).remove();
        }
    });

    $("#select-all").change(function () {
        select_all(this.checked);
    });
});