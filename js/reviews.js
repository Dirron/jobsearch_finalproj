$(document).ready(function () {
    let uri = new URI(window.location);
    let params = uri.search(true);

    display_results();

    function display_results() {
        let company_name = params.company;

        return $.ajax({
            dataType: "json",
            url: "/handlers/get-companies.php",
            data: {company: company_name},
            success: function (data) {
                let results = "";

                $.each(data, function (key, value) {
                    results += format_result(value);
                });

                $("#results").html(results);

                $("#main-container").show();
            },
            error: function(jqxhr, status, exception) {
                console.log("Exception: ", exception);
            }
        });
    }

    function format_result(data) {
        let company_id = data.id;
        let company_name = data.name;
        let address = data.address ? data.address : "";
        let rating = data.rating ? data.rating + "%" : "n/a";
        let salary = data.avg_salary ? "$" + data.avg_salary : "n/a";

        if (address) {
            address = "at " + address;
        }

        return `<a class="list-group-item" id="result-${company_id}">
                    <div class="d-flex flex-row align-items-end"><h2 class="mr-2">${company_name}</h2><h4 class="text-info pb-1">${address}</h4></div>
                    <br/>
                    <div class="d-flex flex-row">
                        <div class="mr-5">
                            <p class="text-info">Approval Rating: ${rating}</p>
                        </div>
                        <div class="">
                            <p class="text-info">Average Salary: ${salary}</p>
                        </div>
                    </div>
                </a>`;
    }
});