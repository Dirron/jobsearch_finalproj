$(document).ready(function () {

    let is_logged_in = false;

    // set the text for the top right button (login/logout)
    $.ajax({
        url: "login.php",
        type: "post",
        data: {function_name: "is_logged_in"},
        success: function (data) {
            let text;
            is_logged_in = data;
            if (is_logged_in) {
                text = "Logout";
            } else {
                text = "Login";
            }
            $("#log-btn").html(text);
        }
    }).then(account_button);

    // top right logout button action
    $("#log-btn").click(function () {
        if (is_logged_in) {
            $.ajax({
                type: "POST",
                url: "login.php",
                data: {account_action: "logout"},
        
                success: function () {
                    window.location.href = "job-search.php";
                }
            });
        } else {
            window.location.href = "login.php";
        }
    });

    function account_button() {
        if (is_logged_in) {
            let button = `<li class="nav-item"><a class="btn btn-link nav-link" href="account.php">Account</a></li>`;
            $("#account-navigation").prepend(button);
        }
    }
});
