<?php

$main = file_get_contents("templates/submit-application.html");

include "tiles/core-js.php";
include "tiles/footer.php";
include "tiles/head.php";
include "tiles/navigation-bar.php";

include "tiles/banner.php";

$post = array();

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if (!isset($_SESSION["email"])) {
    include "helper/login-helper.php";

    login_with_destination("job-search.php", "Please log in to submit an application.");
    exit();
}

if (isset($_POST["submit"]) && isset($_POST["posting_id"])) {
    include "database/application-query.php";
    include "database/user-query.php";

    $user_id = UserQuery::get_id_by_email($_SESSION["email"]);
    $unique_file_id = sha1($_FILES["resume"]["name"] . $_SESSION["email"] . uniqid()) . ".pdf";
    $target_dir = "resumes/";
    $target_file = $target_dir . $unique_file_id;
    $file_type = $_FILES["resume"]["type"];

    if ($file_type == "application/pdf") {
        $moved_file = move_uploaded_file($_FILES["resume"]["tmp_name"], $target_file);
        ApplicationQuery::save($user_id, $_POST["posting_id"], $unique_file_id);

        if ($moved_file) {
            $post["upload_success"] = true;
        }
    }
}

if (isset($_POST["title"]) && isset($_POST["sub_header"]) && isset($_POST["posting_id"])) {
    $post = array_merge($post, array(
        "title" => $_POST["title"],
        "sub_header" => $_POST["sub_header"],
        "posting_id" => $_POST["posting_id"]
    ));
}

// add variables as json to be used by client side
$post_vars_json = json_encode($post);
$post_vars = "<script>var post_vars = $post_vars_json;</script>";
$main = str_replace("{{post_vars}}", $post_vars, $main);

echo $main;