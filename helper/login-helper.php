<?php

function login_with_destination($destination, $message = null) {
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }

    $_SESSION["login_message"] = $message;
    $_GET["dest"] = $destination;
    header("Location: login.php?dest=" . $_GET["dest"]);
}