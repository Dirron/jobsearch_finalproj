<?php

function add_vars_to_js($page, $vars = array()) {
    if (strpos($page, "{{server_response}}") == false) {
        return;
    }

    $server_response = "<script>var server_response = " . json_encode($vars) . ";</script>";

    return str_replace("{{server_response}}", $server_response, $page);
}