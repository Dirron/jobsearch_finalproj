<?php
include_once "../database/company-query.php";
include_once "../database/salary-info-query.php";


if (isset($_GET["company"])) {
    search($_GET["company"] ?? "");
}

function search($company) {

    $approval_ratings = approval_rating();
    $avg_salaries = avg_salaries();

    if (!empty($company)) {
        $data = CompanyQuery::by_name($company)->fetchAll();
    } else {
        $data = CompanyQuery::all()->fetchAll();
    }

    for ($i = 0; $i < count($data); $i++) {
        $row = $data[$i];

        $new = array(
            "id" => $row["id"],
            "name" => $row["name"],
            "address" => $row["address"],
            "rating" => $approval_ratings[$row["id"]] ?? null,
            "avg_salary" => $avg_salaries[$row["id"]]["avg_salary"] ?? null
        );

        $data[$i] = $new;
    }

    echo json_encode($data);
}

function approval_rating() {
    $data = CompanyQuery::reviews()->fetchAll();

    $total = array();
    $approval = array();
    foreach ($data as $row) {
        $company_id = $row["company_id"];
        $prev_approval = isset($approval[$company_id]) ? $approval[$company_id] : 0;
        $prev_total = isset($total[$company_id]) ? $total[$company_id] : 0;

        if ($row["is_approved"]) {
            $approval[$company_id] = ++$prev_approval;
        }

        $total[$company_id] = ++$prev_total;
    }


    foreach($approval as $key => $value) {
        $approval[$key] = $approval[$key] / $total[$key] * 100;
    }

    return $approval;
}

function avg_salaries() {
    $data = SalaryInfoQuery::all_average()->fetchAll();

    return $data;
}