<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if (isset($_POST["function_name"]) && $_POST["function_name"] == "is_logged_in") {
    echo isset($_SESSION["email"]) ? true : false;
    exit();
}

if (isset($_POST["account_action"])) {
    if ($_POST["account_action"] == "logout") {
        logout();
        exit();
    }
}

function login($email, $pass, $dest = null) {
    require "database/mysql-login.php";

    $email = mysqli_real_escape_string($conn, $email);
    $pass = mysqli_real_escape_string($conn, $pass);

    $result = $conn->query("select first_name, last_name, email, password from users where email=\"$email\"");
    if ($result && $result->num_rows == 1) {
        $data = $result->fetch_assoc();
    }

    if (isset($data) && password_verify($pass, $data["password"])) {
        $_SESSION["email"] = $email;
        $_SESSION["fname"] = $data["first_name"];
        $_SESSION["lname"] = $data["last_name"];

        $conn->close();
        if (isset($dest)) {
            header("Location: " . $dest);
        } else {
            header("Location: job-search.php");
        }
    } else {
        $_SESSION["login_message"] = "Invalid email or password";
        header("Location: login.php");
        exit();
    }
}

function logout() {
    session_unset();
    return json_encode(array("success" => true));
}

function register($form) {
    require "database/mysql-login.php";
    require "validators/login-validator.php";

    // on fail: redirect to registration with error
    validate($form);

    $fname = mysqli_real_escape_string($conn, $form["fname"]);
    $lname = mysqli_real_escape_string($conn, $form["lname"]);
    $email = mysqli_real_escape_string($conn, $form["email"]);
    $unhashed_pass = mysqli_real_escape_string($conn, $form["password"]);

    $pass = password_hash($unhashed_pass, PASSWORD_DEFAULT);

    $result = $conn->query("select * from users where email=\"$email\"");
    if ($result && $result->num_rows != 0) {
        header("Location: login.php?error=userexists&email=".urlencode($email));
        exit();
    } else if ($result && $result->num_rows == 0) {
        $result = $conn->query("insert into users (first_name, last_name, email, password)
                                values (\"$fname\", \"$lname\", \"$email\", \"$pass\")");
        
        if (!$result) {
            header("Location: login.php?error=retry");
            exit();
        }

        login($email, $unhashed_pass);
    }
}