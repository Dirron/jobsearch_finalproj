<?php

if (isset($_GET["job"])) {
    search($_GET["job"]);
} else if (isset($_GET["salary_min"]) || isset($_GET["salary_max"])) {
    filter_salary($_GET["salary_min"] ?? null, $_GET["salary_max"] ?? null);
} else if (isset($_GET["job_posting_types"])) {
    job_posting_types();
} else if (isset($_GET["company_id"])) {
    search_by_company($_GET["company_id"]);
} else {
    search();
}

function search ($search_string = null, $location = "", $salary_min = null, $salary_max = null) {
    include "../database/job-query.php";

    if (!empty($search_string)) {
        $query = JobQuery::by_string($search_string, $location);
    } else {
        $query = JobQuery::all();
    }

    if (isset($query)) {
        echo json_encode($query->fetchAll());
    }
}

function search_by_company ($company_id) {
    include "../database/job-query.php";

    if (isset($company_id)) {
        $query = JobQuery::by_company_id($company_id);
    }

    if (isset($query)) {
        echo json_encode($query->fetchAll());
    }
}

function filter_salary($salary_min, $salary_max) {
    include "../database/salary-info-query.php";

    if (!empty($salary_min) && !empty($salary_max)) {
        $query = SalaryInfoQuery::average_within_range($salary_min, $salary_max);
    } else if (!empty($salary_min) && empty($salary_max)) {
        $query = SalaryInfoQuery::average_greater_than($salary_min);
    } else {
        echo "missing parameter";
    }

    if (isset($query)) {
        echo json_encode($query->fetchAll('id', 'company_id, avg(annual_salary) as avg_salary'));
    }
}

function job_posting_types() {
    include "../database/job-posting-types-query.php";

    $query = JobPostingTypesQuery::all();

    echo json_encode($query->fetchAll("id"));
}