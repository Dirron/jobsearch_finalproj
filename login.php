<?php
    include "handlers/login-handler.php";

    if (isset($_SESSION["email"])) {
        header("Location: job-search.php");
        exit();
    }

    if (isset($_POST["account_action"])) {
        if ($_POST["account_action"] == "login") {
            login($_POST["email"], $_POST["password"], $_GET["dest"] ?? null);
        } else if ($_POST["account_action"] == "register") {
            register($_POST);
        }
        exit();
    }

    $main = file_get_contents("templates/login.html");

    include "tiles/core-js.php";
    include "tiles/footer.php";
    include "tiles/head.php";
    include "tiles/navigation-bar.php";

    include "tiles/banner.php";

    $login_message = "";
    if (isset($_SESSION["login_message"])) {
        $login_message_array = array("login_message" => $_SESSION["login_message"]);
        $login_message = "<script> var server_response = " . json_encode($login_message_array) . "; </script>";

        unset($_SESSION["login_message"]);
    }
    $main = str_replace("{{server_response}}", $login_message, $main);

    echo $main;