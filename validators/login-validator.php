<?php
function validate($form) {
    all_fields_filled($form);
    valid_email($form["email"]);
    fields_are_the_same(
        $form["email"],
        $form["email-confirm"],
        $form["password"],
        $form["password-confirm"]
    );
}

function all_fields_filled($form) {
    if (empty($form["fname"])
        || empty($form["lname"])
        || empty($form["email"])
        || empty($form["email-confirm"])
        || empty($form["password"])
        || empty($form["password-confirm"])) {
        
        header("Location: login.php?error=missing");
        exit();
    }
}

function valid_email($email) {
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        header("Location: login.php?error=invalidemail");
        exit();
    }
}

function fields_are_the_same($email1, $email2, $pass1, $pass2) {
    if ($email != $email_conf || $pass != $pass_conf) {
        header("Location: login.php?error=retry");
        exit();
    }
}