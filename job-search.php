<?php

    $main = file_get_contents("templates/job-search.html");

    include "tiles/core-js.php";
    include "tiles/footer.php";
    include "tiles/head.php";
    include "tiles/navigation-bar.php";

    include "tiles/job-search-form.php";

    echo $main;
?>