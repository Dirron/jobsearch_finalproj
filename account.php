<?php

$main = file_get_contents("templates/account.html");

include "tiles/core-js.php";
include "tiles/footer.php";
include "tiles/head.php";
include "tiles/navigation-bar.php";
include "helper/communication-helper.php";

include "tiles/banner.php";

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if (isset($_POST["company_id"])) {
    include "database/job-query.php";

    echo json_encode(JobQuery::count_applications($_POST["company_id"]));
    exit();
}

if (isset($_POST["delete"]) && isset($_POST["job_posting_ids"])) {
    include "database/job-query.php";

    JobQuery::delete($_POST["job_posting_ids"]);
}

if (isset($_SESSION["email"])) {
    include "database/user-query.php";

    $name = $_SESSION["fname"] . " " . $_SESSION["lname"];
    $email = $_SESSION["email"];
    $account_type = UserQuery::get_account_type($email);
    $company = UserQuery::get_company_by_user_email($email);
    $company_name = empty($company["company_name"]) ? "n/a" : $company["company_name"];
    $company_id = empty($company["company_id"]) ? null : $company["company_id"];

    $data = array(
        "name" => $name,
        "email" => $email,
        "account_level" => $account_type,
        "company_name" => $company_name,
        "company_id" => $company_id
    );


    $main = add_vars_to_js($main, $data);
}

echo $main;