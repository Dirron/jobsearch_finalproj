<?php
require_once "query.php";

class JobPostingTypesQuery extends Query {

    public static function all() {
        $fluent = Query::connect();
        return $fluent->from('job_posting_type');
    }

}