<?php
require_once "query.php";

class SalaryInfoQuery extends Query {

    public static function all() {
        $fluent = self::connect();

        return $fluent->from('salary_info');
    }

    public static function average_within_range($min_salary, $max_salary) {
        return self::all()
                    ->select(array('company_id', 'avg(annual_salary)'))
                    ->groupBy('company_id')
                    ->having('avg(annual_salary) >= ? and avg(annual_salary) <= ?', $min_salary, $max_salary);
    }

    public static function average_greater_than($min_salary) {
        return self::all()
            ->select(array('company_id', 'avg(annual_salary)'))
            ->groupBy('company_id')
            ->having('avg(annual_salary) >= ?', $min_salary);
    }

    public static function all_average() {
        return self::all()
            ->select('avg(annual_salary) as avg_salary')
            ->groupBy('company_id');
    }
}
