<?php
require_once "query.php";

class ApplicationQuery extends Query {

    public static function save ($user_id, $job_posting_id, $resume_id) {
        $values = array(
            'user_id' => $user_id,
            'job_posting_id' => $job_posting_id,
            'resume_id' => $resume_id
        );

        $fluent = self::connect();

        return $fluent->insertInto('applications')->values($values)->execute();
    }
}