<?php
require_once "query.php";

class JobQuery extends Query {

    public static function all() {
        $fluent = self::connect();
        $query = $fluent->from('job_postings as j')
                    ->innerJoin('companies as c on c.id = j.company_id')
                    ->select('c.name as company_name')
                    ->where('j.deleted = 0');

        return $query;
    }

    public static function by_string($search_string) {
        $query = self::all()
                    ->where('match(j.description, j.title) against (?) or match(c.name) against (?)', $search_string, $search_string);

        return $query;
    }

    public static function save($company_id, $job_title, $posting_type, $description, $location = null) {
        $fluent = self::connect();

        $values = array(
            'company_id' => $company_id,
            'title' => $job_title,
            'posting_type' => $posting_type,
            'description' => $description,
            'location' => $location
        );

        return $fluent->insertInto('job_postings')
            ->values($values)
            ->execute();
    }

    public static function by_company_id($company_id) {
        return self::all()->where("c.id", $company_id);
    }

    public static function count_applications($company_id) {
        $fluent = self::connect();

        $query_data = $fluent->from('job_postings j')
            ->innerJoin('applications a on a.job_posting_id = j.id')
            ->select('j.title as job_title, count(*) as responses')
            ->groupBy('j.title')
            ->where('j.deleted = 0')
            ->where('j.company_id', $company_id)
            ->fetchAll();
        $data = array();

        foreach ($query_data as $row) {
            $data = array_merge($data, array($row["job_title"] => $row["responses"]));
        }

        return $data;
    }

    public static function delete($job_posting_ids) {
        $fluent = self::connect();

        $set = array('deleted' => 1);

        return $fluent->update('job_postings')
            ->set($set)
            ->where('id', $job_posting_ids)
            ->execute();
    }
}