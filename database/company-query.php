<?php

require_once "query.php";

class CompanyQuery extends Query {

    public static function all() {
        $fluent = self::connect();

        return $fluent->from('companies');
    }

    public static function by_name($company_name) {
        return self::all()
            ->where('match(name) against (?)', $company_name);
    }

    public static function reviews() {
        $fluent = self::connect();
        return $fluent->from('company_ratings cr')
            ->innerJoin('companies c on c.id = cr.company_id')
            ->select('c.id as company_id, c.name as company_name, cr.is_approved');
    }

}