<?php
require_once "query.php";

class UserQuery extends Query {

    public static function get_id_by_email ($email) {
        $fluent = self::connect();

        return $fluent->from('users')
            ->where('email', $email)
            ->fetch()["id"];
    }

    public static function get_account_type ($email) {
        $fluent = self::connect();

        return $fluent->from('users u')
            ->innerJoin('user_types ut on ut.id = u.user_type')
            ->select('ut.description as description')
            ->where('u.email', $email)
            ->fetch()["description"];
    }

    public static function get_company_by_user_email($email) {
        $fluent = self::connect();
        return $fluent->from('companies c')
            ->innerJoin('users u on u.id = c.user_id')
            ->select('c.name as company_name, c.id as company_id')
            ->where('u.email', $email)
            ->fetch();
    }
}