<?php
function fluent() {
    require_once $_SERVER["DOCUMENT_ROOT"] . "/libraries/FluentPDO/FluentPDO.php";

    $hn = 'localhost'; //hostname
    $db = 'oudomsod'; //database
    $un = 'oudomsod'; //username
    $pw = 'oudomsod'; //password
    $pdo = new PDO("mysql:host=$hn;dbname=$db", $un, $pw);

    return new FluentPDO($pdo);
}
?>