-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 16, 2018 at 05:12 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `oudomsod`
--

-- --------------------------------------------------------

--
-- Table structure for table `applications`
--

CREATE TABLE `applications` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `job_posting_id` int(11) NOT NULL,
  `resume_id` varchar(50) NOT NULL,
  `date_created` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `applications`
--

INSERT INTO `applications` (`id`, `user_id`, `job_posting_id`, `resume_id`, `date_created`) VALUES
(2, 1, 1, '0490ce70d1504e8dd3e45bd5e094250813e2abeb.pdf', '2018-08-14 17:44:28'),
(3, 1, 1, 'df4f02dad7729804fe42a5ae08307b717c9dfb10.pdf', '2018-08-14 17:45:30'),
(4, 1, 6, '5ad889cb3071c0365ba160efb8d5586913c2ede1.pdf', '2018-08-15 00:41:06'),
(5, 1, 5, '0ac11f9300e6b3fc0750a8fef1d053158cca5d78.pdf', '2018-08-15 00:41:14'),
(6, 1, 4, '87fe835b25dca42a1a169d5e7457374509381c43.pdf', '2018-08-15 00:41:25'),
(7, 1, 4, '649d14cfa1c7c3344d0fb792006d85cdb2417a9d.pdf', '2018-08-15 00:41:32');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `address` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `user_id`, `name`, `address`) VALUES
(1, 3, 'Bob\'s Burgers', '123 Ocean Avenue'),
(2, NULL, 'Googull', '123 Beach St.'),
(3, 2, 'Make Money Fast', NULL),
(4, 4, 'Dunder Mifflin Scranton', '1725 Slough Avenue, Scranton, PA');

-- --------------------------------------------------------

--
-- Table structure for table `company_ratings`
--

CREATE TABLE `company_ratings` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `is_approved` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_ratings`
--

INSERT INTO `company_ratings` (`id`, `company_id`, `is_approved`, `user_id`) VALUES
(1, 3, 1, 2),
(2, 3, 1, 2),
(3, 3, 1, 2),
(4, 3, 1, 2),
(5, 3, 0, 1),
(6, 4, 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `job_postings`
--

CREATE TABLE `job_postings` (
  `id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `posting_type` int(11) DEFAULT NULL,
  `description` varchar(40000) DEFAULT NULL,
  `date_posted` datetime DEFAULT CURRENT_TIMESTAMP,
  `date_last_modified` datetime DEFAULT CURRENT_TIMESTAMP,
  `location` varchar(128) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_postings`
--

INSERT INTO `job_postings` (`id`, `company_id`, `title`, `posting_type`, `description`, `date_posted`, `date_last_modified`, `location`, `deleted`) VALUES
(4, 3, 'Scammer', 1, 'Earn money off from other people!', '2018-07-03 00:00:00', '2018-07-05 00:00:00', NULL, 0),
(5, 2, 'Expert Computer Guy', 3, 'Machine learn the block chain using neural if statements.', '2018-07-03 00:00:00', '2018-07-05 00:00:00', 'Melbourne, Australia', 0),
(6, 3, 'Big Money Maker', 1, 'Yes you heard right--you will make big money.', '2018-08-14 23:03:26', '2018-08-14 23:03:26', '', 0),
(23, 1, 'Burger Flipper', 2, 'You will flip burgers.<br/>\r\n<br/>\r\nResponsibilities:<br/>\r\n- Flipping burgers<br/>\r\n- Not eating the burgers<br/>', '2018-08-15 18:40:25', '2018-08-15 18:40:25', '123 Ocean Avenue', 0),
(24, 1, 'Dish Washer', 2, 'Clean dishes. Make shiny.<br/>\r\n<br/>\r\nResponsibilities<br/>\r\n- Washing dishes<br/>\r\n- Putting away dishes<br/>', '2018-08-15 18:40:25', '2018-08-15 18:40:25', '123 Ocean Avenue', 0),
(27, 3, 'Tester of Tests', 3, 'Test<br />\r\nthings<br />\r\nin<br />\r\nthe<br />\r\ntesting<br />\r\nlab', '2018-08-15 18:45:57', '2018-08-15 18:45:57', 'Basement', 0),
(28, 1, 'Customer', 1, 'Hello, please come eat at my restaurant.', '2018-08-15 22:49:32', '2018-08-15 22:49:32', '123 Ocean Avenue', 0),
(29, 3, 'CRA Scammer', 3, 'Call people and tell them that they owe money!<br />\r\n<br />\r\nRequirements:<br />\r\n- Lack of morals<br />\r\n- Aggressive voice<br />\r\n- Your own phone', '2018-08-15 22:53:27', '2018-08-15 22:53:27', 'Work from home', 0),
(30, 4, 'Regional Manager', 1, 'I enjoy having breakfast in bed. I like waking up to the smell of bacon, sue me. And since I don&#039;t have a butler, I have to do it myself. So, most nights before I go to bed, I will lay six strips of bacon out on my George Foreman Grill. Then I go to sleep. When I wake up, I plug in the grill, I go back to sleep again. Then I wake up to the smell of crackling bacon. It is delicious, it&#039;s good for me. It&#039;s the perfect way to start the day. Today I got up, I stepped onto the grill and it clamped down on my foot... that&#039;s it. I don&#039;t see what&#039;s so hard to believe about that.', '2018-08-15 23:04:24', '2018-08-15 23:04:24', 'Scranton, Pennsylvania', 0),
(31, 4, 'Accountant', 1, 'Why waste time say lot word when few word do trick?', '2018-08-15 23:06:03', '2018-08-15 23:06:03', 'Scranton, Pennsylvania', 0),
(32, 4, 'Assistant to the Regional Manager', 1, 'I come from a long line of fighters. My maternal grandfather was the toughest guy I ever knew. World War II veteran, killed twenty men, and spent the rest of the war in an Allied prison camp. My father battled blood pressure and obesity all his life. Different kind of fight.', '2018-08-15 23:07:31', '2018-08-15 23:07:31', 'Scranton, Pennsylvania', 0);

-- --------------------------------------------------------

--
-- Table structure for table `job_posting_type`
--

CREATE TABLE `job_posting_type` (
  `id` int(11) DEFAULT NULL,
  `description` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_posting_type`
--

INSERT INTO `job_posting_type` (`id`, `description`) VALUES
(1, 'Full-time'),
(2, 'Part-time'),
(3, 'Temporary');

-- --------------------------------------------------------

--
-- Table structure for table `salary_info`
--

CREATE TABLE `salary_info` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `posted_by_user_id` int(11) DEFAULT NULL,
  `annual_salary` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salary_info`
--

INSERT INTO `salary_info` (`id`, `company_id`, `posted_by_user_id`, `annual_salary`) VALUES
(1, 1, NULL, 30000),
(2, 2, NULL, 45000),
(3, 1, NULL, 33000),
(4, 1, NULL, 31000),
(5, 1, NULL, 35000),
(6, 1, NULL, 35000),
(7, 2, NULL, 35000),
(8, 3, NULL, 85000);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(128) NOT NULL,
  `resume_id` int(11) DEFAULT NULL,
  `user_type` int(11) NOT NULL DEFAULT '3'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `avatar`, `email`, `password`, `resume_id`, `user_type`) VALUES
(1, 'Tina', 'Belcher', NULL, 'jimmyfan@email.com', 'butts', NULL, 3),
(2, 'super', 'super', NULL, 'super@localhost.com', 'super', NULL, 1),
(3, 'Bob', 'Belcher', NULL, 'bob.belcher@burgers.com', 'babyyoucanchivemycar', NULL, 3),
(4, 'Michael', 'Scott', NULL, 'michael.scott@dundermifflin.com', 'paper', NULL, 3);

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE `user_types` (
  `id` int(11) NOT NULL,
  `description` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_types`
--

INSERT INTO `user_types` (`id`, `description`) VALUES
(3, 'Job Seeker'),
(2, 'Recruiter'),
(1, 'Admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `applications`
--
ALTER TABLE `applications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `companies` ADD FULLTEXT KEY `idx_company_name` (`name`);

--
-- Indexes for table `company_ratings`
--
ALTER TABLE `company_ratings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_postings`
--
ALTER TABLE `job_postings`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `job_postings` ADD FULLTEXT KEY `idx_job_description` (`description`,`title`);

--
-- Indexes for table `salary_info`
--
ALTER TABLE `salary_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `applications`
--
ALTER TABLE `applications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `company_ratings`
--
ALTER TABLE `company_ratings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `job_postings`
--
ALTER TABLE `job_postings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `salary_info`
--
ALTER TABLE `salary_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
