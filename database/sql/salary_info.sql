create table salary_info (
    id int not null primary key AUTO_INCREMENT,
    company_id int not null,
    posted_by_user_id int,
    annual_salary float not null
)

insert into salary_info (company_id, annual_salary) values (1, 20000);
insert into salary_info (company_id, annual_salary) values (1, 25000);
insert into salary_info (company_id, annual_salary) values (1, 22000);
insert into salary_info (company_id, annual_salary) values (1, 18000);
insert into salary_info (company_id, annual_salary) values (1, 19000);
insert into salary_info (company_id, annual_salary) values (1, 19500);