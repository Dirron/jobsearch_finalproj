use oudomsod;
create TABLE users (
    id int AUTO_INCREMENT not null primary key,
    first_name varchar(50) not null,
    last_name varchar(50) not null,
    avatar varchar(255),
    email varchar(255) not null,
    password varchar(128) not null,
    resume_id int,
    user_type int not null
);

insert into users(first_name, last_name, email, password, user_type)
values("Tina", "Belcher", "jimmyfan@email.com", "butts", 1);